# Reflections on two-factor authentication
At UiB and Metacenter there is a consent that we use a second factor for
authentication (2FA).
In practice this means [Time-based One-time Password
(TOTP)](https://en.wikipedia.org/wiki/Time-based_One-Time_Password)
using an app on a phone like Aegis or FreeOTP.
This kind of second factor has some security disadvantages.
[Universal 2nd Factor
(U2F)](https://en.wikipedia.org/wiki/Universal_2nd_Factor) is much more
secure when used properly, but this is still not widely known, at least
not around me, and you need a hardware token.
2FA/TOTP is still better than no second factor, at least if this does
not result in negligence of the first authentication factor.

## Recovery
GitLab generates a recovery code for you to save somewhere digitally or
print.
With this code one has access to the second factor (and can choose a new
PIN in their app); the recovery code *is* the second factor.
There is even a print button to make a paper copy easier.
At first I thought to save it in my Password Store using strong
encryption (as we use for [systems'
passwords](https://git.app.uib.no/sc/internal-documentation/-/blob/master/Access-servers.md)).
However, then both the first and the second factor can be accessed
simultaneously defeating the purpose of the second factor.

But what can I do better?
The Password Store is the most secure thing I have digitally.
When I store the recovery code somewhere else digitally but easily
accessible, it will be less secure, because I chose to use pass(1)
because of its security and usability.
I can increase digital security by storing it on a USB stick or my [OpenBSD
laptop](https://git.app.uib.no/mvhulten/my-documentation/-/blob/master/laptop/installation-Latitude-7420.md#self-managed-openbsd)
with the master PGP key (for instance in combination with the pass(1)
utility).

Also printing a recovery code is not such a good idea.
You cannot trust printers.
Actually, the fact that the same web browser on the same computer where
you login with your password shows the recovery code defeats the purpose
if this very computer is compromised.
If you set up 2FA on a very secure (possibly less daily-use) computer
and then use it on a less secure one (e.g. if you have to), it makes
more sense.

Writing down the recovery code from a sufficiently secure system on
paper would be secure and should not be that hard.
But it is hard as you can easily make mistakes writing the code on paper
(and there is no error correction mechanism).
Why is not [BIP-39](https://en.bitcoin.it/wiki/BIP_0039) used for these
codes?
Then you could easily write them down on acid-free paper with a HB
pencil.
I suppose those who argue for using this kind of security would find
TOTP a useless excercise anyway and argue to use U2F (with a BIP-39
recovery seed).
