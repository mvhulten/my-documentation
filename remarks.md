# Small remarks and ideas
## 2022-03-11 – Zoom
For a long time I was [not able to use
Zoom](https://git.app.uib.no/mvhulten/my-documentation/-/issues/37)
under OpenBSD.
Enabling WebAssembly (WASM) on `chromium` made Zoom finally work.

As mentioned
[here](http://daemonforums.org/showthread.php?t=12020#post72152) WASM is
not available in Iridium (probably for good reasons).

I mentioned in the Zoom group meeting today that Zoom is crap because
it's using WebAssembly and not using the standard WebRTC.
Of course, it would even be better if we don't use proprietary
software/services for this at all; a year ago I gave a group meeting
presentation over my Jitsi instance.
This was on a VM in Amsterdam using only 512 MiB back then (now upgraded
to 1 GiB), and that ran quite smoothly.
If the university is so keen on sustainability, why not invest in
technologies of which you know exactly how much resources it needs;
hosting it locally (in Norway) should reduce the carbon dioxide
footprint even more.

There is a significant audio lag, though, probably only on my side
because of Zoom using bad tech (see above) and OpenBSD not being
tested/supported (even though it is the most sane general-purpose
operating system available).

## 2022-02-24 – Blockchain (and Git)
Yesterday I presented on
[blockchain](https://git.app.uib.no/mvhulten/presentation-blockchain).
It is mostly about Bitcoin, because that is the first decentralised
consensus blockchain.
It can be used well as a(n educational) reference for blockchain
technology.
One of the discussions was if version control systems where each new
commit includes a hash of the previous one is also a blockchain.
And if it is decentralised.

Several properties of blockchain could include (among other things):

1. Hashed chain of blocks
2. Decentralisation (independent of any single party)
3. Consensus mechanism (one true chain)

If a blockchain is simply a chain of blocks that are connected somehow,
this would be a technology that predates Bitcoin by far.
One-way hashing is just an obvious way to chain the chunks of data.
It becomes more interesting when we make it decentral.
I've seen different definitions of *decentralised* and *distributed*,
some where one is stronger than the other, some vice versa.
Let us not go into that but only use a somewhat loose definition of
*decentralised* that is consistent with Bitcoin (or at the very least
how it is originally designed).
It means that there is no single point of failure : any participant in
the Bitcoin network could be removed or behave badly without impacting
the trustworthiness to any significant extent.
In June 2021 China banned bitcoin mining, which was clearly visible from
the significant drop in hashing rate.
Bitcoin went on fine albeit for a short period with fewer blocks being
mined per unit of time.
This resolved itself by a difficult adjustment (happening every
fortnight).
Also mining operations were moved elsewhere, but this is not essential
for the resilience of the Bitcoin network.

At the IT department, and elsewhere in the university, we use Git for
many of our code, documentation and other plain text data.
A Git repository is a hashed chain of data blocks (that contain code
diffs, or commits).
It is decentralised in the sense that anyone can download and work on a
repository, but there are some caveats.
Firstly there is, for practical reasons, an authorative, central
repository (hosted at [this instance](https://git.app.uib.no/)).
The *truth* of the repository, including its full history, is determined
by the owner of the repository (and/or, strictly, administrators of the
instance).
Even though the commits are cryptographically linked, history can be
easily rewritten by someone having those rights, as long as there is no
independent truth mechanism (either by a centralised, trusted third
party, or through decentralised consensus) ensuring the future hashes
(with respect of the point of deviation) shall not be changed.
It is not to say this is a bad thing; it is just not truly
decentralised.
Of course, even if the instance is taken away, the repository does not
stop existing—many people may have a copy of the repository, and there
may be different ways to push and pull repositories between each other.

So Git has properties (1) and, to a limited extent, (2).
There is no programmatic consensus mechanism (3).
There is a sort of consensus mechanism, one that is based on trust and
*de facto truth*.
The central repository has an owner, and we also trust the
administrators.
What is really missing from Git being a blockchain (in the strict sense
of having at the minimum the three mentioned properties) is that there
is not a *decentralised consensus mechanism*.
Bitcoin provides this through Proof of Work (PoW), and this is extremely
useful for a *money* and arguable for *contracts* and such.
In software development this would not work very well.
That should be based on merits (and people trusting that other people
have such merits), which cannot be easily—or at all—forged in a
programmatic way.
There is no algorithm for *Proof of Merits*.

One consequence of having properties (1) and (2) but *not* (3) is that
forks can live on as long as some actors want them to.
This is fine for software; it would be horrible for money.

Clearly, current version control systems (those I'm aware of) don't have
a programmatic decentralised consensus mechanism.
If that disqualifies them to be « blockchain » is a matter of
definition.

## 2022-01-10 – Secure by default
Having used [OpenBSD](https://www.openbsd.org/) for some time now, the
the field of IT has become even weirder for me, at least in appearance,
than it already was.
OpenBSD is *secure by default*.
More or less as a consequence, it is not complex (its logic hardly
changes over time) and it is very usable.
Security, complexity and usability are all improved because of this.
One can install OpenBSD and have an understandable, secure system.

Most GNU/Linux systems are different.
For instance, for Red Hat Linux Enterprise (RHEL), SELinux is running in
enforcing mode by default.
Any user or sysadmin who does not want to spend much time and is faced
with some program not working because of SELinux may be inclined to set
it to _permissive_.
It is unclear to me if this leaves the system insecure, or less secure
than before.
I have several systems running without SELinux or a similar security
layer, but I deem them secure anyway.
Am I wrong to assume that, e.g, Devuan GNU/Linux is reasonably secure
at installation and after configuring any added daemons properly?
Is GNU/Linux actually insecure and do we need to rely on additional
layers of security (provided by the NSA)?
Aren't the resources for understanding such extra security layer wasted
resources?

## 2021-10-11 – Guix
There is an [ultimate essay arguing for the existence and use of GNU
Guix](https://ambrevar.xyz/guix-advance/).

## 2021-10-01 – OpenSSH with 2FA
The discussion about closing OpenSSH (tcp/22, hereafter *ssh*) on the
WAN firewall comes up now and then, as it did recently at ITA.
Because it is not using 2FA, supposedly.
Two-factor authentication (2FA) or multi-factor authentication (MFA) is
one of the four pillars of the current *Documentation Sprint* at UiB's
IT department (ITA).
2FA seems like a fashion today, or a dogma, and we should not keep out
of sight that it should improve security significantly without
considering the different options that could be at least as secure,
maintainable and usable.
For our group (Scientific Computing), among others, the question of the
use of a second factor for OpenSSH is an open one.

OpenSSH knows more than a handful of standard authentication methods,
and other methods can be included as well.
The methods we use include standard password, public key and host-based
authentication.
The last two both use public-key cryptography, where the first one is
per-user and discussed here, and the second one is for us only of
interest as a way (for software) to login between compute nodes and is
not further discussed here.
I have demonstrated we could use TOTP as a second factor on top of what
ssh is already providing.

It is good to improve security, and a second factor is very sensible for
many network services where authentication is needed, but it is an open
question how much sense it makes for OpenSSH.
There are two options (that I've explored) to harden security of SSH:
one is good use of public key authentication, another is using a proper
second factor.

### Public key authentication
In a way, public key authentication where there is a passphrase on the
private key is using two factors.
One factor is what you have: the private part of the keypair.
The other factor is what you know: the passphrase to the private key.
There are several caveats:
1. The private key is on the same system as where you enter your
   passphrase, so they are not really independent factors
2. The private key must be managed properly (stored on secure machine)
3. The private key must be protected with a passphrase ("second
   factor").

Concerning (1), this is not unlike how 2FA is already used a lot by many
individuals for important tasks.
For instance, I heard that some people are using for their banking an
app *on the same device as they are initiating and finalising monetary
transactions*.
(In Norway this is called *Mobile BankID* and is different from the more
secure *BankID*.)
Again, same device, thus not an independent second factor.
I am not saying it is fine to copy bad behaviour or infrastructure, but
it is useful to put the potential issue with (1) in perspective.
Even worse, in a way, is the double-wrong authentication that is
typically done at UiB at least for MS Teams.
The first factor is the password that is likely to become compromised by
entering your UiB LDAP password into a website of Microsoft (check the
domain in your web browser when logging in).
For the second factor the proprietary app *Microsoft Authenticator* is
actually
[proposed](https://hjelp.uib.no/tas/public/ssp/content/detail/service?unid=bc42678779674d3092da5345b14bae31)
by the IT department, in lieu of free apps like Aegis as I documented
[elsewhere](https://git.app.uib.no/mvhulten/my-documentation/-/blob/master/telephone/installation-Galaxy-S10.md#second-factor).

To be fair, TOTP has to be set up initially by using information from
the website you want to use it (the QR code you need to scan).
This step implies that the devices are not truly independent.
I have not done the research how likely of an attack vector this setting
up is, but only Universal 2nd Factor (U2F) could mediate that issue, be
it likely or not.

Concerning (2) and (3), this is mostly mediated through educating new
admins as described in [an
issue](https://git.app.uib.no/sc/internal-documentation/-/issues/41).
(Non-admin users are (to be) informed as well, but we cannot rely on
their goodwill alone; i.e. we need to take into account that users might
not handle private keys well.
Note that you technically cannot check if they either manage their keys
securely nor/including if they have protected it with a passphrase (good
or at all).)

I believe therefore that in a trusted culture of knowledgable admins
these caveats are not that critical, and that with proper guidelines and
education the use of public key authentication is sufficiently secure.

Of course, if one can login *also* by password only (using password
authentication), this weakens the potential security advantages of
public key authentication.

### TOTP for OpenSSH
A proper second factor (on a separate device) would be to require a
Time-based One-time Password (TOTP), by many familiar under the name
*Microsoft Authenticator*.

It is possible to use TOTP as a second factor on top of one of the
standard ssh(1) authentication method (see respective section of man
page).
On OpenBSD this is
[straightforward](https://dataswamp.org/~solene/2021-02-06-openbsd-2fa.html)
to set up as I showed in a presentation earlier this year.
I presume that on PAM systems (any GNU distro I know of and some BSDs)
this should be straightforward as well.
Nonetheless, setting up a test system for this and giving a
demonstration is not the same thing as rolling this out on a large scale
and maintaining it.
Security would likely be improved with a second factor, but usability
might suffer.

I believe that a second factor like TOTP would improve a lot of services
significantly, but I do not believe that this is the right approach for
ssh.
Rather we should work on ensure we do public key authentication properly
and improve where needed, and discuss in which cases we must keep
password authentication and how and when we should use a root console.
If and when this is improved and we insist to improve some more, TOTP
will probably look like an even less useful option and the next step
should be U2F.

## 2021-09-08 – shebang space
For portability, after the shebang (`#!`), `/usr/bin/env` should follow
the name of the interpreter, before the name of the shell.
In practically all operating systems `/usr/bin/env` exists.
But what about using a space after the shebang?

    #! /usr/bin/env perl

[Apparently](https://www.in-ulm.de/~mascheck/various/shebang/#blankrequired)
there has been a Unix, 4.1 BSD, where it was required.
Now it is optional, and uncommon for that matter.

## 2021-09-09 – `xeyes` considered essential for cat
Historically the example program `xeyes` has been part of the XFree86
and X.Org reference implementations.
I found this a useful utility simply for testing if and how smoothly X11
is working, including remotely (over OpenSSH).
So called "minimal" or source-based package managers and GNU
distributions often split up (modularise) large programs and as such
these utilities are not installed together with the X11 suite.
This is the case in [GNU Guix](https://guix.gnu.org/) where one should
define `xeyes` in the user's
[manifest](https://guix.gnu.org/cookbook/en/html_node/Basic-setup-with-manifests.html)
or simply through

    guix package -i xeyes

Working at home I found there was a use for `xeyes` beyond testing the
proper functioning of your computer.
As often the cat sat in front of my screen and became intrigued by
moving objects thereon.
When I subsequently opened `xeyes` and moved around the cursor, virtual
eyes following, the cat became obsessed and seemed to think that my
screen is a touch screen.
Nonetheless it is not, and thus the cat did not succeed in taking
control over the virtual pupils.
