TOPdesk works in Firefox; use the [operator
login](https://hjelp.uib.no/tas/secure/).

## Create and respnse to TOPdesk incidents
TOPdesk sends HTML e-mails by default.
This is undesirable and we cannot change the default because it is
proprietary software.
(Actually, back in the 2000s I asked *OGD Software*, who held the
copyright back then, to fix a problem with pressing escape while in
editting mode exiting a ticket without saving.
They did not *want* to do this because it was not a problem for most
users.
They are unfriendly to free software users.)
With the following procedure these are the consequences, addressing
respective issues:

- plain text
- template: less typing
- you have your own archive
- escape issue resolved

### Procedure
1. (Create a TOPdesk incident and) copy the ticket number to the
clipboard.
2. Copy the `TEMPLATE.??.md` file to `TICKETNUMBER-YYYYMMDD.md` and
edit the latter file, including the ticket number.
3. Copy the body text to the clipboard (when using `vim` with wordwrap,
do it in full screen).
4. Paste it into the reply field in TOPdesk and click Save/Lagre.
5. Copy the link to the ticket from the body and paste in the text file.
6. Switch to plain text and remove the body.
7. Copy the whole text file and paste in TOPdesk and send the e-mail.

