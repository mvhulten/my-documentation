# Installation of Dell Latitude 7420
The laptop finally arrived today and comes installed with Ubuntu 20.04.
This is good, because we now know Ubuntu, standard at ITA, runs on it.
But it needs to be reinstalled, because

1. We want standard ITA Ubuntu on it for
    - user support
    - out-of-box Wi-Fi, pull printers, ...
    - a bit more secure (controlled by ITA, less chance of backdoors)
2. I want to run OpenBSD on it as well
    - master key management (security)
    - daily operation (security & usability)

## BIOS
Today a [critical
update](https://www.dell.com/support/home/en-us/product-support/product/latitude-14-7420-2-in-1-laptop/drivers)
for the BIOS.
At delivery 1.6.1 is installed.
As of 21 June there is an important [BIOS
update](https://www.dell.com/support/home/en-us/drivers/driversdetails?driverid=3gy1x&oscode=ubt20&productcode=latitude-14-7420-laptop),
version 1.7.1, which addresses (Intel CPU) vulnerabilities, Type-C dock
recognision, amongst other things.
Apropos, on an ITA-drift Ubuntu machine, the Ubuntu Software Center
should be able to see that the dock needs an upgrade (mine went from
version 43.00 to 60.00 and should have addressed issues with screens,
though I did not see any change; I can still only connect up to two
external screens, no matter if running Ubuntu or OpenBSD).

> Once the BIOS is upgraded, you cannot downgrade the BIOS EXE/Recovery
> (RCV) to versions earlier than 1.4.1.

Since the laptop works now, with 1.6.1 (F2 to boot in BIOS), it's fine
to upgrade to 1.7.1.
It's almost always good to update to the latest BIOS, even as they are
proprietary, because your computer is usually less insecure after an
upgrade, and also because after some time manufacturers will remove the
blobs.

Upgrading the BIOS "on a Linux" (sic) used to be impossible, but it is
possible now with [this
procedure](https://www.dell.com/support/kbdoc/en-us/000131486/update-the-dell-bios-in-a-linux-or-ubuntu-environment#updatebios2015).
The *UEFI capsule update* utility is referring to the `fwupd` package,
available from the Ubuntu repository.
To install software without a command line, use *Ubuntu Software* of
which an icon can be found at the left of your screen.
However, at the point of writing (24 June) the 1.7.1 update is not (yet)
available from [fwupd](https://fwupd.org/lvfs/devices/) for the Latitude
7420, thus we have to use the more involved method (USB stick).

In theory one should follow [Dell's flashing
manual](https://www.dell.com/support/kbdoc/en-us/000131486/update-the-dell-bios-in-a-linux-or-ubuntu-environment#updatebios2015),
but the first part is unnecessarily involved and the BIOS menus look
different to me, so the following may be more helpful.
Put a GPT partition type W95 FAT32 (LBA) (type c) on a USB stick and format
it on Linux with `mkfs.vfat -n Dell-BIOS /dev/sdaX` with *X* the partition number.
On OpenBSD run (assuming `sd2` to be your USB device)
```
fdisk -e sd2
sd2: 1> reinit gpt
sd2*: 1> edit 0     # select 0C, use defaults, write an exit
newfs_msdos -L DellBIOS /dev/rsd2i
mount /dev/sd2i /mnt
cp path/to/Latitude_7X20_1.??.?.exe /mnt/
```
Mount it and put the [latest
BIOS](https://dl.dell.com/FOLDER07442326M/1/Latitude_7X20_1.7.1.exe) on
it.
Check the checksum, unmount and restart the laptop.
Start the *One Time Boot Menu* (BIOS configuration) by pressing F12 at
boot.
Blue text are clickable.
Flash the system by clicking on *BIOS UPDATE* and click on
*Flash from file* and browse to the right partition (for me it is called
*Dell-BIOS,.../CDROM(0X...*) and browse to the right file (for instance,
`Latitude_7x20_1.7.1.exe`).
Be sure to have the power cable connected and confirm to upgrade.
<img src="updating-BIOS.jpg" alt="Updating BIOS" width="400"/>
It will also upgrade Intel ME; we're stuck with that for any modern
Intel machine.
Wait until it finishes and boots to Ubuntu's login screen.
Type `dmesg | grep BIOS` to check if the BIOS has been updated.

## Operating systems
It is not adviced to do any serious work on the laptop as it is
delivered by Dell with their modified version of Ubuntu 20.04, because

- it may contain backdoors (in addition to what can be expected on any
  Intel system),
- you won't be able to enjoy the Eduroam Wi-Fi connected on the official
  client VLAN, NFS mounts and paths and pull printers without a
  significant effort,
- if testing or support on the client level is needed an official ITA
  drift installation is good to have,
- the laptop is more or less unusable.

As for the latter, this may be personal taste, at least I cannot work
with it as it is.
It starts an interface (I think GNOME 3) that has some icons on the left
that do not contain a link to a terminal application.
If you click on the menu button in the left bottom corner (9x9 squares)
and type in `xterm` nothing comes up.
Finally, pressing Alt-Ctrl-F{1,2,...} does not do anything, so no
terminal, so no control.
Without any affinity with the new "vision" of programmers of Generation
Y/Z one is lost.

Still, Ubuntu, ITA-driven or not, is not good enough when it comes to
high security.
Therefore, the laptop will be dual-boot installed with OpenBSD.
OpenBSD can be used for master GPG keys and the like, but could also
work as very usable day-to-day driver.
For support (and security, though not really applicable in this specific
case) reasons, ITA is averse to dual boot.
This is understandable, and you will be responsible for dual-boot
working properly and keeping both operating systems up-to-date.

There is *no* 8P8C (often referred to as "RJ45") connector on this
laptop (nor will there likely on any newer models).
Network boot does not recognise the WD19TB dock, so borrow a USB-C
network dongle from Geir (or ask Gunn).
The MAC address can be made unique *to the laptop* by setting *System
Uniqe MAC Address* in the BIOS.
However, OpenBSD (at least `bsd.rd`) does not recognise or conform to
this BIOS setting, so we will want to register the dock (at the office
where we want to get an IP address from `129.177.0.0/16`).
Make a [TOPdesk](support/TOPdesk.md) ticket for this:.

> Kan disse MAC-adressene registreres i Foreman?
> 
>     60:18:95:18:8d:a4 (primaer, dock)
>     38:14:28:ab:fa:d2 (sekondaer, System Unique MAC Address)

where the primary address is that of the dock and the secondary the
system address that can be used for Ubuntu but not by OpenBSD.

Possibly you should first enter the BIOS by pressing [F2] at boot-up and
[enable PXE
boot](https://www.dell.com/support/kbdoc/en-us/000131551/bios-settings-to-allow-pxe-boot-on-newer-model-dell-latitude-laptops).
Go to *Boot Configuration*.
Disable *Secure Boot Mode*.
It will complain that "Secure Boot may reduce the security of your
system", but Secure Boot makes no sense in most situations.
Firstly, it resticts your hardware, making you less free and potentially
less secure.
Secondly, remote attack vectors are unlikely, especially if you keep
your operating system up-to-date and sufficiently secure, as well as
maybe your BIOS.
In the case of local access you should consider your system compromised
anyway, except, *maybe*, in the case of full-disk encryption in
combination with a free BIOS (like Libreboot).
You could consider disabling *Hyperthreading* under *Performance*.
This will make your system slower, but the technology is insecurely
designed.
OpenBSD has disabled it by default, and since we will install OpenBSD as
the only real secure system (and we might want to test performance for a
user under Ubuntu), I decided to leave it enabled.
Do keep you microcode up-to-date if you leave it enabled (included in OS
upgrades).

### ITA-driven Ubuntu
In BIOS settings we need to set (following Ole Arntzen):
+ UEFI-boot
+ Secure boot: Off
+ Integrated devices: USB/Thunderbold Configuration: *Enable USB Boot
  Support* (on the right)!

**More options in boot menu.. not clear which is best.**

Select in boot menu (F12): *UIEFI PXEv4 (MAC:registered-mac-address)*.

PXE boot didn't work for me.
I went to Geir who can boot the system from a USB, apparently specially
crafted (maybe [like
this](https://wiki.uib.no/itwiki/index.php/Ubuntu_installasjon#Installasjon_ved_bruk_av_minnepinne))
because my USB sticks that should be bootable are not, and they are
prepared with network installation options for ITA Ubuntu.
This worked.
Hereafter the machine must be registered in Foreman, e.g. by Geir Hansen
or Ole Arntzen, to be able to login.
And you must ask full sudo (`root`) access if you need this.

For help, make a TOPdesk ticket or start a thread in team 3.4 (*Nett- og
klientdrift*) i MS Teams.

Even if you got a US layout keyboard, the default ITA Ubuntu
installation comes with Norwegian layout.
Do this to change in GUI and on console:
- Add a keyboard layout in settings within Gnome, then you can change it
  at right top corner; GDM will also use your new keyboard layout.
- In console (Press **fn-ctrl-alt-F3**) login using Norwegian kb layout
  and change layout with `sudo dpkg-reconfigure keyboard-configuration`
and keep kb model, but *Country of origin* must be changed to *English
(US)* to be able to select the *English (US)* keyboard layout in the
third screen (I keep AltGr as AltGr and set *Left Logo key* for compose;
of course Alt-Ctrl-Backspace should kill X); run `setupcon`.

Remove software that is either insecure or is very annoying, and install
useful software:

    apt-get update
    apt-get upgrade
    apt-get remove bluez.* btrfs-progs command-not-found gdm3 gnome-bluetooth thunderbird.*O
    apt-get autoremove
    rm -rf /var/lib/command-not-found
    apt-get install atril cdo chromium-browser claws-mail-{acpi-notifier,address-keeper,archiver-plugin,attach-remover,attach-warner,bogofilter,feeds-reader,newmail-plugin,vcalendar-plugin} conky eom ferret-vis ffmpeg fig2dev fonts-{dejavu,font-awesome,lmodern,mathjax} gajim gajim-omemo gfortran gnuplot guvcview gv imagemagick inkscape ipython3 julia mate-{power-manager,utils} mlocate mpv nco ncview netcdf-bin nmap pandoc pmount python3-{axolotl-curve25519,cdo,dev,ferret,gnupg,matplotlib,numpy} rxvt-unicode sent spectrwm sshuttle suckless-tools texlive texlive-{extra-utils,fonts-recommended,latex-recommended} ttf-dejavu-extra vim vrms xdm xterm youtube-dl zathura

### Self-managed OpenBSD
After Ubuntu is installed and you can login, you'll find a huge
`/dev/mapper/system-scratch` filesystem.
We will reduce its size to make place for an OpenBSD and a shared data
GPT partition (henceforth *disk slice*).
The latter is needed because OpenBSD cannot access partitions from LVM
(nor can it mount ext4 with journaling which `system-scratch` happens
to be) and Linux cannot access partitions from the OpenBSD slice (nor
can it mount the FFS2 filesystem).
Ironically, NTFS is likely the most reliable filesystem to share between
OpenBSD and Linux.
I have a 1 TB disk; you may want to choose different numbers for a
different disk size.

```
sudo -i
umount /scratch
e2fsck -f /dev/mapper/system-scratch
resize2fs /dev/mapper/system-scratch 90G    # reduce ext4 filesystem
lvresize -L90G /dev/mapper/system-scratch   # reduce LVM partition
#partprobe /dev/nvme0n1                     # part of gparted / not sure if needed
lvscan                                      # infer total size
pvresize --setphysicalvolumesize 201.76G /dev/nvme0n1p3
cfdisk /dev/nvme0n1                         # LVM slice -> 202G
```

I created in the free space a 295G *OpenBSD data* slice and a 456G
*Microsoft basic data* slice.

Get and prepare image file; under GNU/Linux like this:

    dd if=install69.img of=/dev/sdb bs=1M

New laptops don't support legacy boot anymore but use EFI boot instead.
You may check this by checking if the filesystem on the first slice
includes `/efi/boot/bootx64.efi`.
Now when starting boot menu, it shows *UEFI USB Flash Disk*.
Press Enter to start the OpenBSD installer.
I selected default choices (Enter), except for:
- I had to select `ure1`, which is the Ethernet interface from the USB-C
  dongle.
  In section 4 of the [OpenBSD man pages](http://man.openbsd.org/) you'll
  find information about the different interfaces: generalise from our
  case ure(4).
- DNS domain name: `klientdrift.uib.no`.
- Allow root ssh login?  `prohibit-password`.
- I made these disklabel partitions within the OpenBSD slice:

| slice     | size      | mount point   |
| --------- | --------- | ------------- |
| a         | 5g        | `/`           |
| b         | 32.1g     | swap          |
| d         | 50g       | `/usr`        |
| e         | 10g       | `/var`        |
| f         | 5g        | `/tmp`        |
| h         | rest      | `/home`       |

So we should have the 32 GiB needed for hybernation if we would want to,
and there's plenty for ports and building base system.
Install all file sets and reboot the system.

### Dual-boot Ubuntu and OpenBSD
The installation will not have touched the MBR.
We will let GrUB manage the boot process, which is managed in turn by
Ubuntu.
The easiest is to just chainload OpenBSD by adding this entry to
`/etc/grub.d/40_custom`:

```
menuentry "OpenBSD" {
        insmod part_gpt
        insmod chain
        set root='(hd0,gpt4)'
        chainloader (hd0,gpt4)/BOOTX64.EFI
}
```

Make sure that the GrUB menu waits for you, edit `/etc/default/grub` and
set:

```
GRUB_TIMEOUT=4
GRUB_CMDLINE_LINUX_DEFAULT=""
```

Then run `update-grub && reboot`.

Before adding this, you may want to put version control over Ubuntu's
`/etc/` to track changes.

The file `/usr/mdec/BOOTX64.EFI` on the OpenBSD system must be made
available to the root filesystem.
Boot again from the installation medium, mount the root and usr
filesystems (start installation and drop to shell with Ctrl-Z) after
selecting the root disk):

    mount /dev/sd0a /mnt
    cd /mnt
    mount /dev/sd0d usr
    cp usr/mdec/BOOTX64.EFI ./
    cd
    umount /mnt/usr
    umount /mnt
    reboot

At the moment of this writing (2021-07-08), the kernel boots but then
the system freezes (well, actually the fan goes wild trying not to melt
it).
Let's upgrade to the newest snapshot: put the snapshot amd64 `bsd.rd` in
`/` and boot it:
- Download it from
  [here](https://ftp.eu.openbsd.org/pub/OpenBSD/snapshots/amd64/) and
  save it on the Ubuntu `/boot` partition (Linux cannot mount OpenBSD
  partitions and OpenBSD cannot mount LVM partitions nor can it mount NTFS
  partitions without the not-yet-installed respective fuse library).
- Boot the USB stick again, or use `bsd.rd` from the installed system
  (simply select OpenBSD from GrUB and boot `bsd.rd`).
- Select *Upgrade* (skipping irrelevant config bits).
- Exit to shell with Ctrl-C after the detection of the disks (`mknod`s
  the disk's partitions for you), mount the partition where you've put
  `bsd.rd`, rename the current `bsd.rd` and copy the new one in the new
  system's root (e.g. `/mnt/` if you mounted it there:

```
disklabel sd0
mount /dev/sd0j /mnt2   # check your partition with above output
umount /mnt
mount /dev/sd0a /mnt    # clean remount writable was needed
cd /mnt
mv bsd.rd bsd.rd.orig
cp /mnt2/bsd.rd ./
umount /mnt /mnt2
reboot
```

- Select OpenBSD again from grub and boot `bsd.rd`.
- Upgrade!

If it fails again, cancel the installation (Ctrl-C), unmount and check
the root filesystem (`fsck -f /dev/sd0a`) and start the upgrade again by
entering `upgrade`.

First loading the OpenBSD bootloader before actually booting may not be
strictly needed, but GrUB is not very stable (hence [GRUB is reported to
usually fail](https://www.openbsd.org/faq/faq4.html)).
Chainloading it like this should be stable (assuming GrUB or Ubuntu
don't change the API).

### Disk encryption
OpenBSD's full-disk encryption does not work in a multi-boot
environment.
Possibly there are other ways to do full-disk encryption in a multi-boot
environment, but that would leave a less standard ITA-driven Ubuntu and
probably OpenBSD doesn't provide easy hooks for this.
The most important is that we have the option to secure certain files
among which our GnuPG master keypair (even though that will already be
passphrased); hence we will encrypt our `/home` partition (conveniently
called `sd0h` in our partition scheme).

...

To recap, on the laptop's internal disk there is an OpenBSD GPT
partition (`sd0` in OpenBSD language) that includes different slices,
one of which (`sd0h`) is a softraid partition that will be called `sd1`
after typing in your passphrase, and then you can mount this full
virtual disk as `sd1c`.

## Other tips
There is a webcam shutter: use it.
Interestingly,
> the 7000 models can only be configured with LTE, not 5G, and the
> optional webcam shutter is manual, lacking the new automated system of
> the high-end models.

The whole idea of a webcam shutter is that you have control over it
because you don't trust the proprietary components and software that
Dell, Intel and the Chinese put into it.
Good thing it is *not* automated for the 7000 series!

Audio through HDMI [does not
work](https://www.reddit.com/r/BSD/comments/j2nfxm/why_do_the_bsds_not_support_audio_over_hdmi_when/)
on OpenBSD.
For ITA *filmkveld*, instead use the mini jack.
