# Bugs in Ubuntu
This is an overview of bugs and annoyances present on the Latitude 7420
laptop (`tron`) when running Ubuntu (ITA managed).
Several of these may apply to a wider range of GNU/Linux distributions.

 - in a virtual console, tmux(1) lines are printed at places suggesting
   that tmux is not respecting $LINES
 - the ethernet interface gets in the right subnet but I still need
   sshuttle(1) to be able to act as if I'm in the UiB network
 - Claws Mail interface it not refreshed to current state (work-around
   is switch between workspaces)
 - there are errors during init; verbosity level is usually horrible
 - takes long time to boot
 - debugging is difficult because of systemd; it is too complex
 - external screen interfaces (HDMI or DisplayPort) are not recognised
 - *many* man pages are missing
    - even man pages that are referred to by installed man pages often
      don't exist, e.g. realpath(1) suggests the respective page from
      section 3, but it doesn't exist
    - most parts of the system are not documented, e.g. no manpages for
      most in `/etc`
 - the APIs of many software components are changing regularly, often
   without any apparent good reason

All these things work properly under OpenBSD.
