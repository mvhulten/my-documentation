# Choosing a good work mobile phone
The IT department at UiB provides three brands of smartphones:

1. Apple *iPhone*
2. Samsung *Galaxy*
3. CAT *Cat Phone*

The iPhone is probably reasonably secure if kept updated, but there is
no way to check this because it almost everything in the iPhone
(hardware to software) is proprietary.
It is known to have backdoors; there are probably more backdoors we
don't know about.
When Apple deems the phone to be obsolete, there are probably no ROMs
available that may extend the lifetime significantly.
They have generally high specifications, but I'm sure an Android or Cat
Phone would suffice in day-to-day activities.
We have a laptop, workstation and/or HPC for more serious computation.

Apple products should not be provided by the UiB because of UiB's
outspoken stance on the Sustainable Development Goals (SDGs) and general
ethical considerations.
Be wary about information to the contrary: greenwashing and such abound.
In addition the IT department should not provide Apple products because
we cannot control them.
At the very least we need to be able to have control over our computing.

The Samsung Galaxy runs a (partly) proprietary Android operating system,
but there are alternatives (ROMs) available for installation that
provide more freedom and privacy.

The Cat Phone is for made endurance.
It comes with its own (proprietary) operating system.
The phone's producers have shown uncooperative in providing source code
or specifications.

For maximal security, privacy and freedom I'd advice a Samsung Galaxy.
It comes with Android, including proprietary drivers, a proprietary
overlay (*One UI*), the proprietary Google Play Services and several
proprietary (Samsung) apps that you cannot easily remove.
This guide will show you how to take control over this telephone.

You should care about the machine's *security* because you will probably
want to use it as a second authentication factor.
In addition, you'll be storing your collegues contact information (at
least phone numbers) on it, and it's good for the privacy of your
collegues to not do that on machines you cannot trust.
The latter can be mitigated on the other hand by getting a work
telephone number from the UiB (also good for work/life balance).
You might care about *privacy* yourself, especially considering we
sometimes tend to mix work and private life.
Finally, *freedom* and *control* can be valuable even when only speaking
of productivity.
Maximising *control* may also result in working more efficiently.
Also consider getting a work phone number from the UiB to have a good
work/private balance.

Order your *Android* telephone on
https://shop.techstep.no/produkter/mobiltelefon/mobiltelefoner/ (login
can be found on hjelp.uib.no).
When you are logged in, you will only find phones and accessoires that
you are allowed to order.
I went for the Galaxy *S10*, because the successor *S20* model could not
be delivered, the *S21* was not really better than previous models, and
of all these three models the *S10* looked the most sane to me (e.g. it
still has a mini-jack interface), and still modern enough with the new
USB-C interface.
Additionally, recently LineageOS has been released for the S10 and is
not officially available yet for S20 or higher.
As a concequence it would be more difficult to reliably get control over
the more recent models (at the moment).

## Physical features of Galaxy S10
- Model number – SM-G973F/DS
- Land code – NEE (Nordic countries)
- XDA Developers code name – beyond1lte
- Chipset 64bit – Samsung Exynos 9 Octa 9820 (8nm)
- CPU processor – Octa-Core, 3 processors: 2.7Ghz Dual-Core Mongoose M4 2.3Ghz Dual-Core ARM Cortex-A75 1.9Ghz Quad-Core ARM Cortex-A55
- GPU graphical controller – ARM Mali-G76 MP12
- RAM memory – 8GiB LPDDR4X
- Disk space – 128 GiB
