# Installation of the Galaxy S10
Many of the notes here are likely useful for other Samsung Galaxy phones,
but it is tested only on the **Galaxy S10** (model number: SM-G973F/DS)
with the **Exynos 9820** system-on-chip (SoC).

Most Samsung Galaxy phones support many ROMs.
Sadly on recent models the free Android distribution
[Replicant](https://replicant.us/), does not work (fully).
However, [Lineage OS](https://lineageos.org/) runs properly on most
Galaxy devices and can provide increased freedom, security, control,
usability and privacy compared to stock Android or what Samsung
provides.
Since August 2021 an official LineageOS ROM is available for *S10*.

For the *Galaxy S20*, have a look at
[Exynoobs](https://exynoobs.github.io/OTA/).

## Rooting the phone
In case you want to [root](https://en.wikipedia.org/wiki/Rooting_(Android))
the phone, refer to the [TWRP installation
manual for the Galaxy S10](https://twrp.me/samsung/samsunggalaxys10.html).

## Installing LineageOS
Be sure to install the right versions of all the software, i.e. for the
Exynos (*beyond1lte*) model.

Follow the official instructions
[here](https://wiki.lineageos.org/devices/beyond1lte/install).
Additional notes (when not clear in the linked instructions or specific
to my situation):
- Firmware version can be found in settings → About phone → Software
  information → Baseband version.
- Concerning booting to bootloader: the Bixby button is the one below
  Volume Down (on the left side of the phone).
- To enable or check OEM unlocking, first enable Developer Mode (About
  Phone → Software → tap Build number seven times) and then check in
  Developer options (OEM unlocking must be enabled and finally grayed
  out).
- The PGP check only makes sense if the author's identity can be
  [checked](http://pgp.mit.edu/pks/lookup?search=asn%40redhat.com) and
  he is [known](https://www.samba.org/~asn/) in the community for
  writing useful software.
- Instead of the official recovery I have used TWRP version
  `twrp-3.3.1-0-android-10.0-beyond1lte-1.img`, following [these
  steps](https://forum.xda-developers.com/t/recovery-official-3-5-0-x-twrp-for-galaxy-s10-e-5g-exynos.4180287/),
  but the LineageOS recovery is fine as well.
- I flashed from a GNU Guix system with packages `adb fastboot heimdall
  libusb` installed.

### Troubleshooting
I had a couple of issues during the installation of an unofficial ROM
from XDA Developers (when there was no official LineageOS release yet);
some of these key combinations were helpful [0,1].

| Key combination           | Purpose           |
| ------------------------- | ----------------- |
| VolDown+Power (>= 7 s)    | reboot/halt       |
| VolUp+VolDown *hereafter* | really halt       |
| VolDown+Bixby+USB         | *Download Mode*   |
| VolUp+Bixby+Power         | recovery mode     |

- *Download Mode* is Samsung's version of bootloader mode, where you can
  flash a ROM (using `fastboot`).
  It is also referred to as *Odin Mode*, referring to Samsung's
  proprietary [flashing
  software](https://en.wikipedia.org/wiki/Odin_(firmware_flashing_software)).
  Heimdall is a free replacement that runs on GNU/Linux.
- *Recovery mode* is used for factory data reset, clear cache, flash
  official update package and can backup and restore whole ROM and data
  [2].
- At some point I could not even shutdown the phone using the first two
  key combinations.
  I could get into bootloader mode by executing the first and third
  lines subsequently.

[0]: https://forum.xda-developers.com/t/recovery-exynos-twrp-3-5-2_9-for-s10e-s10-s10-update-2021-06-07.4067765/
[1]: https://forum.xda-developers.com/t/why-s10-no-booting-into-download-mode.3937302/
[2]: https://forum.xda-developers.com/t/difference-between-bootloader-download-and-recovery-mode.3661049/

## Installing apps
Instead of Google Play Store the free software app store
[F-Droid](https://f-droid.org/) will be installed.

So far I have been able to avoid proprietary apps, but if you do need
anything from Google Play Store (be it proprietary or free), use [Aurora
Store](https://f-droid.org/en/packages/com.aurora.store/).
It may be needed to have root in a way some apps do not notice.
For this you need
[Magisk](https://f-droid.org/en/packages/com.topjohnwu.magisk/).
It is not just an app, but it entails rooting your phone among other
things.
I have neither tested nor used Aurora and/or Magisk on this phone.

### Second factor
For Two Factor Authentication (2FA) use
[Aegis](https://f-droid.org/en/packages/com.beemdevelopment.aegis/) or
[FreeOTP+](https://f-droid.org/en/packages/org.liberty.android.freeotpplus/).
These are probably the most secure and reliable apps for this purpose.
Here I'm using Aegis.
Let us add a second factor from Teams (if you already are using the
*Authenticator app* and want to redo this properly, best to remove the
current entry as they will look indistinguisable when you want to remove
the old one after these steps):
- Go to myaccount.microsoft.com (e.g. from Teams → Manage account)
- Open [Security info](https://mysignins.microsoft.com/security-info)
- *Add methods* → *Authenticator app*, *Add*
- *I want to use a different authenticator app*.

Now it says to set up your account in the app, so
- Start Aegis and set a password (store in your password vault)
- When clicking *Next* on your PC, you get a QR code to scan with Aegis
  and touch *Save*.

### Communication
The modem works and connects to the cellular network; standard telephone
and SMS are usable.
Note that the modem is proprietary and cannot be disabled.

We are using [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/)
for e-mail.
Relevant accounts should be set up, for most of us:
- [UiB](https://it.uib.no/en/Email_setup_on_private_computers) (Send:
  SSL/TLS)
- For Metacenter, use `{imap,smtp}.gmail.com` with your
  username`@metacenter.no` and respective password (Send: using UiB SMTP
  works).

To set up the accounts in K-9 Mail, go to *Settings* → *Add account*.

There are no free apps for Zoom, Teams and Slack.
For chat and video calling, no open standards are used by either UiB or
Metacenter, so no need to do more on the phone.
I usually use a web browser on my [laptop](laptop) for this.

#### Using the telephone's Internet on your laptop
You can connect a USB-C cable (USB-A on one side works as fine) between
the telephone and the laptop.
Then select *USB tethering*.
On OpenBSD `ifconfig` shows a new interface, called `urndis0` (in my
case).
Now just `sh /etc/netstart urndis0` and you're good to go.

I checked with `traceroute` and
[RIPE](https://apps.db.ripe.net/search/query.html) where the packets go
through.
If your Wi-Fi is enabled, it uses that connection, otherwise it falls
back to your carrier (Telia).
